import time
from selenium.webdriver import Firefox, FirefoxProfile, Chrome
from selenium.webdriver.firefox.options import Options

class BotAnysite:

    def __init__(self):
        
        self.browser = Chrome("")
        self.auth_yandex()
        
    def auth_yandex(self):
        self.browser.get('https://yandex.ru/')
        self.browser.find_element_by_link_text("Войти в почту").click()
        self.browser.find_element_by_name("login").click()
        time.sleep(10)  # Ждет 10 сек
        # Ввод логина
        self.browser.find_element_by_name("login").send_keys("myrlylogin")
        # Ввод пароля
        self.browser.find_element_by_name("passwd").send_keys("myrly_pass")
        # Жмем войти
        self.browser.find_element_by_class_name("passport-Button-Text").click()
        time.sleep(10)
        self.anysite()

    def anysite(self):
        self.browser.get('https://anysite.ru/')
        time.sleep(5)
        self.browser.find_element_by_xpath("/html/body/div[3]/div/div/div/div/div[2]/div[2]/span").click()
        self.browser.get('https://anysite.ru/login')
        self.browser.find_element_by_xpath("/html/body/div/div/div/div/form/div/div[3]/div/span[1]/a/span").click()
        time.sleep(10)
        self.message_sender()

    def message_sender(self):
        obj_list = ["https://anysite.ru/list0bjects1",
                    "https://anysite.ru/list0bjects1"]
        for element in obj_list:
            self.browser.get(element)
            self.browser.find_element_by_xpath(
                "/html/body/div[2]/div[2]/div[2]/div[2]/div/div[2]/div/div[2]/div[2]/span") \
                .click()
            time.sleep(10)
            self.browser.find_element_by_class_name("ChatInput__textarea")\
                .send_keys(
                "Какой-то текст")
            self.browser.find_element_by_link_text("отправить сообщение").click()
            print(element)

def main():
    BotAnysite()

if __name__ == '__main__':
    main()
